import { createRouter, createWebHistory } from 'vue-router'
import HelloWorld from './components/HelloWorld.vue'
import MainPage from './components/MainPage.vue'
import HistoryPage from './components/HistoryPage.vue'
import AccountPage from './components/AccountPage.vue'

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: MainPage,
    },
    {
        path: '/history',
        component: HistoryPage,
      },
      {
        path: '/account',
        component: AccountPage,
      }
      

  ]
})